var DataTypes = require("sequelize").DataTypes;
var Sequelize = require("sequelize").Sequelize;
var _mahasiswa = require("./mahasiswa");
var _nilai = require("./nilai");
const sequelize = new Sequelize('latihan1', 'root', '', {
  host: 'localhost',
  dialect: 'mysql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

function initModels(sequelize) {
  var mahasiswa = _mahasiswa(sequelize, DataTypes);
  var nilai = _nilai(sequelize, DataTypes);


  return {
    mahasiswa,
    nilai,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
