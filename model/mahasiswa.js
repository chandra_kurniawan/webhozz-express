// const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mahasiswa', {
    nik: {
      type: DataTypes.STRING(10),
      allowNull: true,
      primaryKey:true
    },
    nama: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    kelas: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    // sequelize,
    tableName: 'mahasiswa',
    timestamps: false
  });
};
