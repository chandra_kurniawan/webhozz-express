
const {DataTypes} = require("sequelize");
const {Sequelize} = require("sequelize");
const sequelize = new Sequelize('latihan1', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.mahasiswa=require('./mahasiswa')(sequelize,DataTypes)
db.nilai=require('./nilai')(sequelize,DataTypes)

module.exports= db
